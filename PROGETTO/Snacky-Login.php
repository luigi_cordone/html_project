<?php

include 'php/db_connect.php';
include 'php/login_utilities.php';


$errors = "";
sec_session_start(); 
if(isset($_SESSION['tipologia'])){
if($_SESSION['tipologia'] == 'Cliente'){
				header('Location: Snacky-Home.php');
				exit();
			}
if($_SESSION['tipologia'] == 'Fattorino'){
				header('Location: Fattorini-Home.php');
				exit();
			}
if($_SESSION['tipologia'] == 'Fornitore'){
				header('Location: Fornitori-Home.php');
				exit();
			}
		header('Location: Snacky-Home.php');
				exit();
			
}
if( isset($_COOKIE['username']) && isset($_COOKIE['password'] )){
	login($_COOKIE['username'], $_COOKIE['password'], $mysqli);
	if($_SESSION['tipologia'] == 'Cliente'){
				header('Location: Snacky-Home.php');
				exit();
			}
}
if(isset($_POST['username'], $_POST['p']) && !strlen($_POST["p"]) < 2) { 
   $username = test_input($_POST['username']);
   $password = test_input($_POST['p']); // Recupero la password criptata.
    if(login($username, $password, $mysqli) == true) {
			// Login eseguito
			if(isset($_POST['rememberMe']) && $_POST['rememberMe'] == 'Yes'){
	setcookie('username',$username,time()+60 * 60 * 24 * 365);
	setcookie('password',$password,time()+60 * 60 * 24 * 365);
			}
			echo 'Success: You have been logged in!';
			if($_SESSION['tipologia'] == 'Cliente'){
				header('Location: Snacky-Home.php');
				exit();
			}
			if($_SESSION['tipologia'] == 'Fattorino'){
				header('Location: Fattorini-Home.php');
				exit();
			}
			if($_SESSION['tipologia'] == 'Fornitore'){
				header('Location: Fornitori-Home.php');
				exit();
			}
			header('Location: Snacky-Home.php');
				exit();
			} 
}

 ?>




<!DOCTYPE html>
<html lang="it">
<head>
	<title>Snacky-Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="scripts/notify.min.js"></script>
	<script src="scripts/sha512.js"></script>
	<script src="scripts/scriptLogin.js"></script>
	<link rel="stylesheet" type="text/css" href="css/login.css">	
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>


	<!-- Sezione di Login -->
<section class="login-block">
    <div class="container">
	<div class="row">
		<div class="col-md-4	 login-sec">
		
		<?php
			if(strlen($errors)!=0){
				
				?>
		<div id = "phpAlert" class="alert alert-danger alert-php" role="alert"  style="margin-top:5%">
					Errore durante il login!
					<p><?=$errors?></p>
				</div>
			<?php
			}
			?>
		<div class="alert alert-danger alert-js" role="alert" style="display: None">
					Dati inseriti non corretti
					<p></p>
				</div>
		    <h2 class="text-center">Effettua il Login</h2>
			
			<form id = "myForm" class="login-form" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  <div class="form-group">
    <label for="inputUsername" class="text-uppercase">Username</label>
    <input type="text" id="inputUsername" placeholder="Username..." name="username" class="form-control" placeholder="">
 
  </div>
  <div class="form-group">
    <label for="inputPassword" class="text-uppercase">Password</label>
    <input type="password" id="inputPassword" placeholder="Password..." name="password" class="form-control" placeholder="">
  </div>
  
    <div class="form-check">
	
    <label id = "label" for="inputRemember" class="form-check-label">
      <input type="checkbox" id="inputRemember" name="rememberMe" class="form-check-input" value = "Yes">
      <small>Ricordami</small>
    </label>
	
	
    <button id="submitButton" type="submit" class="btn btn-login float-right">Accedi</button>
	
	<a id="registerButton" href="Snacky-Registration.php" class="btn btn-default">Non hai un account? Registrati subito!</a>
	</div>
			</form>
		</div>
		
		
	<!-- Banner Laterale -->
		<div class="col-md-8 banner-sec">
            <div id="slideBanner" class="carousel slide" data-ride="carousel">
                 <ol class="carousel-indicators">
                    <li data-target="#slideBanner" data-slide-to="0" class="active"></li>
                    <li data-target="#slideBanner" data-slide-to="1"></li>
                    <li data-target="#slideBanner" data-slide-to="2"></li>
                  </ol>
            <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img class="d-block img-fluid" src="https://i.imgur.com/QzK7ae2.jpg" alt="Banner Pubblicitario 1">
      <div class="carousel-caption d-none d-md-block">
        <div class="banner-text">
            <h2></h2>
            <p></p>
        </div>	
  </div>
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="https://i.imgur.com/qE0eOcm.jpg" alt="Banner Pubblicitario 2">
      <div class="carousel-caption d-none d-md-block">
        <div class="banner-text">
            <h2></h2>
            <p></p>
        </div>	
    </div>
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="https://i.imgur.com/BLQVmZu.jpg" alt="Banner Pubblicitario 3">
      <div class="carousel-caption d-none d-md-block">
        <div class="banner-text">
            <h2></h2>
            <p></p>
        </div>	
    </div>
  </div>
            </div>	   
		    
		</div>
	</div>
</div>
</div>
</section>
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white" style="opacity:0.9;">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Login.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		 <!-- NAVBAR  -->
		 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Registration.php" title="Registrati">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">group_add</i>
					</button>
					
				</a>
				</li>
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Login.php" title="Effettua il login">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">input</i>
					</button>
					
				</a>
				</li>
			
		
			
			</ul>
		 </div>
	</nav>
	
	

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
