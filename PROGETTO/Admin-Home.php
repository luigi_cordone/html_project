<?php
include 'php/login_utilities.php';
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "snackydb";
$check = false;
$errors = "";
sec_session_start(); 

?>

<!DOCTYPE html>
<html lang="it">
<head>
	<title>Snacky-Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/panel.css">	
	<link href="css/font-awesome.css" rel="stylesheet" />
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>


	<!-- per inserire dentro il riquadro bianco,  inserire gli elementi sotto il div row-->
<section class="login-block">
    <div class="container">
	<div class="row">

    </div>
  </div>
          
</section>
<section class="login-block">
    <div class="container">
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		 <!-- NAVBAR  -->
		 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
		
		
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="" title="Vai alla Home">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">account_box</i>
					</button>
					
				</a>
				</li>
			
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Logout.php" title="Effettua il Logout">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">exit_to_app</i>
					</button>
					
				</a>
				</li>
		
				
			</ul>
		 </div>
	</nav>
	
	<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
                     <h2 id = "title" >PANNELLO ADMIN    <i class="fa fa-cog"></i></h2> 
					 <h3 id = "name"> <b>Nome utente:</b>  <?=$_SESSION['username']?></h3 >
					 <h3 id = "type"> <b>Tipologia Account:</b> <?=$_SESSION['tipologia']?> </h3 >
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
          
                            <div class="row text-center pad-top">
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="Admin-Ordini.php" >
 <i class="fa fa-circle-o-notch fa-5x"></i>
                      <h4>Ordini</h4>
                      </a>
                      </div>
                     
                     
                  </div> 
                 
                  
				   <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="Admin-Charts.php" >
<i class="fa fa-file-text-o fa-5x"></i>
                      <h4>Charts</h4>
                      </a>
                      </div>
                     
                     
                     
                  </div>
				  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="Admin-Utenti.php" >
<i class="fa fa-users fa-5x"></i>
                      <h4>Utenti</h4>
                      </a>
                      </div>
                     
                     
                     
                  </div>
				
                     
                     
                  </div>
                 
              </div>
                 <!-- /. ROW  --> 
               
                  <!-- /. ROW  -->    
                 
                 <!-- /. ROW  -->  
                 
                  <!-- /. ROW  -->  
               
                 <!-- /. ROW  -->   
				  
                  <!-- /. ROW  --> 
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
