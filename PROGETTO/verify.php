<?php

include 'php/db_connect.php';
include 'php/login_utilities.php';

$errors = "";
$activated = false;
if(isset($_GET['hash']) && !empty($_GET['hash'])){
    $hash = test_input($_GET['hash']); // Set hash variable
	  if ($stmt = $mysqli->prepare("SELECT email, chiaveAttivazione, Abilitato FROM accounts WHERE chiaveAttivazione = ? ")) { 
      $stmt->bind_param('s', $hash); 
      $stmt->execute(); 
      $stmt->store_result();
	  if($stmt->num_rows > 0){
		  $stmt = $mysqli->prepare("UPDATE accounts SET Abilitato='1' WHERE  chiaveAttivazione = ? ");
		  $stmt->bind_param('s', $hash); 
		  $stmt->execute(); 
		  $activated = true;
	  }
	  else{
		  $errors = $errors . "La chiave non è associata a nessun account. <br/>";
	  }
}else{
    $errors = $errors . "I dati non sono stati ricevuti correttamente dal server. <br/>";
}
}
else{
    $errors = $errors . "I dati non sono stati ricevuti correttamente dal server. <br/>";
}
?>




<!DOCTYPE html>
<html lang="it">
<head>
	<title>Snacky-Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/verify.css">	
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>


	<!-- per inserire dentro il riquadro bianco,  inserire gli elementi sotto il div row-->
<section class="login-block">
    <div class="container">
	<div class="row">
	<div class="col-md-12		 login-sec">	
	<?php
			if(strlen($errors)!=0){
				
				?>
		<div id = "phpAlert" class="alert alert-danger alert-php" role="alert"  style="margin-top:5%">
					Errore durante l'attivazione!
					<p><?=$errors?></p>
				</div>
			<?php
			}
			?>
		<?php
		
		if(strlen($errors)==0 && $activated){
				
				?>
		<div id = "phpSuccess" class="alert alert-success" role="alert"  style="margin-top:5%">
					Attivazione compiuta con successo, adesso puoi utilizzare le tue credenziali per effettuare il login.
					<p><?=$errors?></p>
					
				</div>
				<a id="loginButton" href="Snacky-Login.php" class="btn btn-default">Torna alla schermata di Login</a>
			<?php
			}
			?>
	</div>
    </div>
  </div>
          
</section>

	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		 <!-- NAVBAR  -->
		 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Registration.html">SIGN UP</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Login.html">LOG IN</a>
			  </li>
			  <li class="nav-item">
				<button type="submit" class="btn btn-light cart" formaction="#"> <!-- In formaction devo specificare URL dove andare. Ho usato button per metter l'Img-->
					<i class="material-icons Responsive">local_grocery_store</i>
				</button> 
			  </li>
			</ul>
		 </div>
	</nav>
	
	

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
