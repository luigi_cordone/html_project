<?php
include 'php/login_utilities.php';
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "snackydb";
$check = false;
$errors = "";
sec_session_start(); 
$text = "";
if (isset($_POST['submit'])) {
	$conn = new mysqli($servername, $username, $password, $database);
	if ($stmt = $conn->prepare("SELECT Abilitato FROM accounts WHERE idAccount = ? LIMIT 1")) { 
      $stmt->bind_param('i', $_POST['submit']); 
      $stmt->execute(); 
      $stmt->store_result();
	  $stmt->bind_result($state);
      $stmt->fetch();
	  if($state == 0){
		  abilita($_POST['submit']);
		  $text = "Account abilitato con successo. Il proprietario dell'account è stato avvisato.";
	  }else{
		  disabilita($_POST['submit']);
		  $text = "Account disabilitato con successo. Il proprietario dell'account è stato avvisato.";
	  }
	
   
	 
  }

}
  
  
  function abilita($idOrdine) {
					$servername="localhost";
					$username ="root";
					$password ="";
					$database = "snackydb";
					$conn = new mysqli($servername, $username, $password, $database);
					$var = "Completato";
					$sql = "UPDATE accounts SET Abilitato = 1 WHERE idAccount=".$idOrdine;
					mysqli_query($conn, $sql);
  }
  function disabilita($idOrdine) {
					$servername="localhost";
					$username ="root";
					$password ="";
					$database = "snackydb";
					$conn = new mysqli($servername, $username, $password, $database);
					$var = "Completato";
					$sql = "UPDATE accounts SET Abilitato = 0 WHERE idAccount=".$idOrdine;
					mysqli_query($conn, $sql);
  }

 ?>


<!DOCTYPE html>
<html lang="it">
<head>
	<title>Utenti - Admin</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/panel.css">	
	<link href="css/font-awesome.css" rel="stylesheet" />

	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body id = "imp">


	<!-- per inserire dentro il riquadro bianco,  inserire gli elementi sotto il div row-->
	<div class="container">
			<div class="container" style="background-color: white; margin-bottom: 2%; border-radius: 10px;">
	<div class="row">
		<?php
			if(strlen($text)!=0){
				
				?>
		<div id = "phpAlert" class="alert alert-success" role="alert"  style="margin:auto;margin-top:1%; ">
	
					<p><?=$text?></p>
				</div>
			<?php
			}
	?>
	 <div class="col-lg-12">
                     <h2 id = "title" >Snacky Database - Utenti    <i class="fa fa-cog"></i></h2> 
					 <h3 id = "name"> <b>Nome utente:</b>  <?=$_SESSION['username']?></h3 >
					 <h3 id = "type"> <b>Tipologia Account:</b> <?=$_SESSION['tipologia']?> </h3 >
                    </div>
	<?php
					$servername="localhost";
					$username ="root";
					$password ="";
					$database = "snackydb";

					echo "<table class = 'table-responsive table table-striped table-bordered' style = margin-top:2%>";
					echo "<tr><th>ID Utente</th><th>Username</th></th><th>Email</th><th class = 'cellulare'>Cellulare</th><th>Stato Account</th><th>Abilita Account</th><th>Disabilita Account</th></tr>";
					$state = "Completato";
					$conn = new mysqli($servername, $username, $password, $database);
					if ($stmt = $conn->prepare("SELECT idAccount,username,email,cellulare,Abilitato,Abilitato FROM accounts "));
					$stmt->execute(); 
					$res = $stmt->get_result();
					while($row = $res->fetch_assoc() ) {
						$stato = "";
						if($row["Abilitato"] == 0){
							$stato = "Disattivato";
							echo "<tr><td>" . $row["idAccount"] . "</td><td>" . $row["username"] . "</td><td>" . $row["email"] . "</td><td>". $row["cellulare"] ."</td><td>". $stato ."</td><td><form action=# method=POST><button type=submit name=submit class= 'btn btn-success' value=". $row["idAccount"] ."> Abilita Account </button></form></td><td>". "</td></tr>";
						}else{
							$stato = "Attivato";
							echo "<tr><td>" . $row["idAccount"] . "</td><td>" . $row["username"] . "</td><td>" . $row["email"] . "</td><td>". $row["cellulare"] ."</td><td>". $stato ."</td><td><td><form action=# method=POST><button type=submit name=submit class= 'btn btn-danger' value=". $row["idAccount"] ."> Disabilita Account </button></form></td>". "</tr>";
						}
						

						
					}
					$conn->close();
					echo "</table>";
					?>

    </div>
  </div>
       </div>   

<section class="login-block">
    <div class="container">
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
			 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
		
	
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Admin-Home.php" title="Vai alla Home">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">account_box</i>
					</button>
					
				</a>
				</li>
				
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Logout.php" title="Effettua il Logout">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">exit_to_app</i>
					</button>
					
				</a>
				</li>
		
				
			</ul>
		 </div>
	</nav>
	
	<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                  
           
                     </div>
         </section>           
</div>
                     
                     
                  </div>
                 
              </div>
                 <!-- /. ROW  --> 
               
                  <!-- /. ROW  -->    
                 
                 <!-- /. ROW  -->  
                 
                  <!-- /. ROW  -->  
               
                 <!-- /. ROW  -->   
				  
                  <!-- /. ROW  --> 
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
