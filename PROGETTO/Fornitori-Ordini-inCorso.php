<?php
include 'php/login_utilities.php';
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "snackydb";
$check = false;
$errors = "";
sec_session_start(); 

?>

<!DOCTYPE html>
<html lang="it">
<head>
	<title>Ordini in corso - Fornitori</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/panel.css">	
	<link href="css/font-awesome.css" rel="stylesheet" />
		<script src="scripts/notify.min.js"></script>
	<script src="scripts/scriptNotifyFornitori.js"></script>
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>


	<!-- per inserire dentro il riquadro bianco,  inserire gli elementi sotto il div row-->
	<div class="container-fluid">
			<div class="container" style="background-color: white; margin-bottom: 2%; border-radius: 10px;">
	<div class="row">
	
	
	 <div class="col-lg-12">
                     <h2 id = "title" >ORDINI EVASI    <i class="fa fa-cog"></i></h2> 
					 <h3 id = "name"> <b>Nome utente:</b>  <?=$_SESSION['username']?></h3 >
					 <h3 id = "type"> <b>Tipologia Account:</b> <?=$_SESSION['tipologia']?> </h3 >
                    </div>
	<?php
					$servername="localhost";
					$username ="root";
					$password ="";
					$database = "snackydb";

					echo "<table class = 'table-responsive table table-striped table-bordered' style = margin-top:2%>";
					echo "<tr><th>ID Ordine</th><th>Data</th><th>Metodo Pagamento</th></th><th>Fatturato</th><th>Stato Ordine</th><th>Cliente</th></tr>";
					$state = "Completato";
					$conn = new mysqli($servername, $username, $password, $database);
					if ($stmt = $conn->prepare("SELECT idOrdine,data,metodoPagamento,costoTotale,username,statoOrdine FROM ordine JOIN accounts on ordine.idCliente = accounts.idAccount WHERE statoOrdine != ? AND idNegozio = ?"));
					$stmt->bind_param('si', $state,$_SESSION['user_id']); 
					$stmt->execute(); 
					$res = $stmt->get_result();
					while($row = $res->fetch_assoc() ) {
						echo "<tr><td>" . $row["idOrdine"] . "</td><td>" . $row["data"] . "</td><td>" . $row["metodoPagamento"] . "</td><td>". $row["costoTotale"] ."€</td><td>" . $row["statoOrdine"] . "</td><td>". $row["username"] . "</td></tr>";

						
					}
					$conn->close();
					echo "</table>";
					?>

    </div>
  </div>
          
</section>
<section class="login-block">
    <div class="container">
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		 <!-- NAVBAR  -->
			 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
		
		<li class="nav-item">
				<div class="dropdown show">
				<a class="nav-link text-white decorated-linkText-nav" href="" title="Le tue notifiche">
				<button  type="button" class="btn btn-light cart btn btn-danger" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <!-- In formaction devo specificare URL dove andare. Ho usato button per metter l'Img-->
					<i class="material-icons Responsive">announcement</i>
					<div id = "space" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
  

  </div>
				</div>
		
				</button> 
				
				
				</a>
				
				
				</li>
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Fornitori-Home.php" title="Vai alla Home">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">account_box</i>
					</button>
					
				</a>
				</li>
				
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Logout.php" title="Effettua il Logout">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">exit_to_app</i>
					</button>
					
				</a>
				</li>
		
				
			</ul>
		 </div>
	</nav>
	
	<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                  
           
                     </div>
         </section>           
</div>
                     
                     
                  </div>
                 
              </div>
                 <!-- /. ROW  --> 
               
                  <!-- /. ROW  -->    
                 
                 <!-- /. ROW  -->  
                 
                  <!-- /. ROW  -->  
               
                 <!-- /. ROW  -->   
				  
                  <!-- /. ROW  --> 
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
