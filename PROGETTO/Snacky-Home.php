<?php
include 'php/login_utilities.php';
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "snackydb";
$check = false;
$errors = "";
sec_session_start();

// Se non è stato effettuato il login, reindirizza alla pagina di login.
if(!isset($_SESSION['tipologia'])){
		header('Location: Snacky-Login.php');
		exit();
	
} else{
	// Se il login è stato effettuato, reindirizza alla homepage relativa alla tipologia utente.

			if($_SESSION['tipologia'] == 'Fattorino'){
				header('Location: Fattorini-Home.php');
				exit();
			}
			if($_SESSION['tipologia'] == 'Fornitore'){
				header('Location: Fornitori-Home.php');
				exit();
			}
			if($_SESSION['tipologia'] == 'Amministratore'){
				header('Location: Admin-Home.php');
				exit();
			}
}
	if(isset($_POST['search']) && !strlen($_POST["search"]) < 2){
	
		$shopId;
		$shop = $_POST['search'];
		$conn = new mysqli($servername, $username, $password, $database);
		
		if($conn->connect_errno){
			echo "Error: impossible to connect. Error number " . $conn->connect_errno . " (" . $conn->connect_error . ")";
		}
		
		$query_SQL = "SELECT nomeNegozio,idAccount FROM accounts";
		$result = $conn->query($query_SQL);

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc() ) {
				if($row['nomeNegozio'] == $shop){
					$check = true;
					$shopId = $row['idAccount'];
				}
			}
			
		}
		
		if($check == true){
			$conn->close();
			header("location: Snacky-Restaurant.php?type=provider&shop=$shopId");
		}else{
			$errors = "errore";
		}
		$conn->close();
	}

?>

<!DOCTYPE html>
<html lang="it">
<head>
	<title>Snacky-Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="scripts/scriptHomeCarousel.js"></script>
	<link rel="stylesheet" type="text/css" href="css/HomePage.css">
	<script src="scripts/notify.min.js"></script>
	<script src="scripts/scriptNotifyClienti.js"></script>
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	
	

</head>


<body>

	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
	
		 <!-- NAVBAR  -->
		 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
		<li class="nav-item">
				<div class="dropdown show">
				<a class="nav-link text-white decorated-linkText-nav" href="Cart.php" title="Prodotti nel carrello">
				<button  type="button" class="btn btn-light cart btn btn-danger" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <!-- In formaction devo specificare URL dove andare. Ho usato button per metter l'Img-->
					<i id ="icon" class="material-icons Responsive">announcement</i>
					<div id = "space" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
			

  </div>
				</div>
		
				</button> 
				
				
				</a>
				
				
				</li>
			 
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Cart.php" title="Prodotti nel carrello">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">local_grocery_store</i>
					</button>
					
				</a>
				</li>
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Home.php" title="Vai alla Home">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">account_box</i>
					</button>
					
				</a>
				</li>
		
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Logout.php" title="Effettua il Logout">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">exit_to_app</i>
					</button>
					
				</a>
				</li>
		
				
			</ul>
		 </div>
	</nav>

	

	 <!-- CAROSELLO IMMAGINI  -->
      <div id="demo" class="carousel slide" data-ride="carousel">

		  <!-- Indicators -->
		  <ul class="carousel-indicators">
			<li data-tarPOST="#demo" data-slide-to="0" class="active"></li>
			<li data-tarPOST="#demo" data-slide-to="1"></li>
			<li data-tarPOST="#demo" data-slide-to="2"></li>
			<li data-tarPOST="#demo" data-slide-to="3"></li>
			<li data-tarPOST="#demo" data-slide-to="4"></li>
		  </ul>

		  <!-- The slideshow -->
		  <div class="carousel-inner">
			
			<div class="carousel-item active">
			  <img src="./img/hamburger.jpg" alt="Hamburger">
				<div class="carousel-caption">
					<h3>Hamburger</h3>
					<p>The best hamburger selection.</p>
				</div>   
			</div>
			
			<div class="carousel-item">
			  <img src="./img/Sandwich.jpg" alt="Sandwich">
				<div class="carousel-caption">
					<h3>Sandwich</h3>
					<p>Create the sandwich you want.</p>
				</div>   
			</div>
			
			<div class="carousel-item">
			  <img src="./img/piadina.jpg" alt="Piadina">
				<div class="carousel-caption">
					<h3>Piadina</h3>
					<p>The original piadina from Cesena.</p>
				</div>   
			</div>
			
			<div class="carousel-item">
			  <img src="./img/pizza.jpg" alt="Pizza">
				<div class="carousel-caption">
					<h3>Pizza</h3>
					<p>The original italian pizza.</p>
				</div>   
			</div>
			
			<div class="carousel-item">
			  <img src="./img/cookie.jpg" alt="Dessert">
				<div class="carousel-caption">
					<h3>Dessert</h3>
					<p>Give yourself a moment of sweetness.</p>
				</div>   
			</div>
			
		  </div>

		  <!-- Left and right controls -->
		  <a class="carousel-control-prev" href="#demo" data-slide="prev">
			<span class="carousel-control-prev-icon"></span>
		  </a>
		  <a class="carousel-control-next" href="#demo" data-slide="next">
			<span class="carousel-control-next-icon"></span>
		  </a>
		</div>
		
		
		
	<!-- Creo una barra di ricerca -->
		
	<div class="topnav">
	<?php
		if((strlen($errors)!=0)) {
	?>
		<div class="container errorPopup" style="display:none;">
		  <div class="alert alert-warning" style="width: 50%; margin-left: auto; margin-right:auto;">
			<p style="text-align:center;color: black;"><strong>Warning!</strong> Il fornitore immesso non è presente.</p>
		  </div>
		</div>
		
		<script>
			$("div.errorPopup").fadeIn();
				setTimeout(function(){  
                    $("div.errorPopup").fadeOut("Slow");  
				}, 4000);  
		</script>
		
		<?php
		
	}
		
	?>
	  <div class="search-container">
		<form action="Snacky-home.php" method="POST">
	
		  <label>Hai già un fornitore di fiducia? </label>
		  <input type="text" placeholder="Inserisci il fornitore" name="search" required>
		  <button type="submit"><i class="material-icons">search</i></button>
		</form>
	  </div>
	</div>
	
	
	
	<!-- Da questo punto aggiungo le immagini cliccabili che reindirizzano ai diversi fornitori -->
	<!-- Tutte le informazioni relative al tipo di prodotto cliccato sono inviate mediante metodo POST -->
	<section>
      <div class="container providerRedirect">
        <div class="row align-items-center">
          <div class="col-lg-6 order-lg-2">
            <div class="p-5">
              <a class="burgerProviders imgProviders" href="Snacky-Restaurant.php?type=HAMBURGER"><img class="img-fluid rounded-circle img-responsive" src="img/miniBurger.jpg" alt="Hamburger"></a>
            </div>
          </div>
          <div class="col-lg-6 order-lg-1">
            <div class="p-5">
              <h2 class="display-4" style='color:black;'>Hamburger</h2>
              <p style='color:black;'>Clicca sulla figura per visualizzare tutti i fornitori affiliati.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
	
	
	<section>
      <div class="container providerRedirect">
        <div class="row align-items-center">
          <div class="col-lg-6">
            <div class="p-5">
              <a class="sandwichProviders imgProviders" href="Snacky-Restaurant.php?type=SANDWICH"><img class="img-fluid rounded-circle img-responsive" src="img/miniSandwich.jpg" alt="Sandwich"></a>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="p-5">
              <h2 class="display-4" style='color:black;'>Sandwich</h2>
              <p style='color:black;'>Clicca sulla figura per visualizzare tutti i fornitori affiliati.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
	
	
	<section>
      <div class="container providerRedirect">
        <div class="row align-items-center">
          <div class="col-lg-6 order-lg-2">
            <div class="p-5">
              <a class="piadinaProviders imgProviders" href="Snacky-Restaurant.php?type=PIADINA"><img class="img-fluid rounded-circle img-responsive" src="img/miniPiada.jpg" alt="Piadina"></a>
            </div>
          </div>
          <div class="col-lg-6 order-lg-1">
            <div class="p-5">
              <h2 class="display-4" style='color:black;'>Piadina</h2>
              <p style='color:black;'>Clicca sulla figura per visualizzare tutti i fornitori affiliati.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
	
	<section>
      <div class="container providerRedirect">
        <div class="row align-items-center">
          <div class="col-lg-6">
            <div class="p-5">
              <a class="pizzaProviders imgProviders" href="Snacky-Restaurant.php?type=PIZZA"><img class="img-fluid rounded-circle img-responsive" src="img/miniPizza.jpg" alt="Pizza"></a>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="p-5">
              <h2 class="display-4" style='color:black;'>Pizza</h2>
              <p style='color:black;'>Clicca sulla figura per visualizzare tutti i fornitori affiliati.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
	
	<section>
      <div class="container providerRedirect">
        <div class="row align-items-center">
          <div class="col-lg-6 order-lg-2">
            <div class="p-5">
              <a class="dessertProviders imgProviders" href="Snacky-Restaurant.php?type=DESSERT"><img class="img-fluid rounded-circle img-responsive" src="img/miniDessert.jpg" alt="Dessert"></a>
            </div>
          </div>
          <div class="col-lg-6 order-lg-1">
            <div class="p-5">
              <h2 class="display-4" style='color:black;'>Dessert</h2>
              <p style='color:black;'>Clicca sulla figura per visualizzare tutti i fornitori affiliati.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
