<?php
include 'php/login_utilities.php';
sec_session_start()	;
$errors = "";
$sommaTot=0;

//ORA E DATA CORRENTE DELL'ORDINE
$now = new DateTime();
$date=$now->format('Y-m-d');    
$time=$now->format('H:i:s');

if(isset($_POST['success'])){
	header('Location: insert.php');
	exit();
}

if(isset($_POST['CONSEGNA'])){
	header('Location: Consegna.php');
	exit();
}

$ora=$_POST['ora'];
$luogo=$_POST['luogo'];
$_SESSION['ora']=$ora;
$_SESSION['luogo']=$luogo;
 ?>



<!DOCTYPE html>
<html lang="it">
<head>
	<title>Snacky-Payment</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	
		<script src="scripts/notify.min.js"></script>
	<script src="scripts/scriptNotifyClienti.js"></script>
	<script src="scripts/scriptPayment.js"></script>
	<link rel="stylesheet" type="text/css" href="css/template.css">	
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>

<section class="login-block">
    <div class="container">


	<div class="col-md-4" id ="pag">
	
		<?php
			if(strlen($errors)!=0){
				
				?>
		<div id = "phpAlert" class="alert alert-danger alert-php" role="alert"  style="margin-top:5%">
					Errore durante la compilazione!
					<p><?=$errors?></p>
				</div>
			<?php
			}
			?>
		<div class="alert alert-danger alert-js" role="alert" style="display: None">
					Dati inseriti non corretti
					<p></p>
				</div>
	
	
	
	

	<div class="panel panel-default credit-card-box">
		<div class="panel-heading display-table" >
			<div class="row display-tr" >
				<h3 id = "tit" class="panel-title display-td" >PAGAMENTO</h3>
					<div class="display-td" >                            
						<img id = "card" class="img-responsive pull-right"  href="#"> <img id = "card" alt="CREDIT_CARD" src="./img/credit_cart.png" width="250" height="40">
					</div>
			</div>                    
		</div>
	
	
		<div class="panel-body">
			<form role="form" id="payment-form">
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="cardNumber">NUMERO CARTA</label>
								<div class="input-group">
									<input 
										type="tel"
										id= "inputNumber"
										class="form-control"
										name="cardNumber"
										placeholder="Valid Card Number"
										autocomplete="cc-number"
										required autofocus 
									/>
									<span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
								</div>
						</div>                            
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="cardExpiry"><span class="hidden-xs">SCADENZA</span></label>
								<input 
									type="tel" 
									id= "inputDate"
									class="form-control" 
									name="cardExpiry"
									placeholder="MM / YY"
									autocomplete="cc-exp"
									required 
								/>
						</div>
				</div>
				
					<div class="col-md-5">
						<div class="form-group">
							<label for="cardCVC">CVV</label>
								<input 
									type="tel"
									id= "inputCode"	
									class="form-control"
									name="cardCVC"
									placeholder="CVV"
									autocomplete="cc-csc"
									required
								/>
						</div>
					</div>
				</div>
						
				<div class="row" style="display:none;">
					<div class="col-xs-12">
						<p class="payment-errors"></p>
					</div>
				</div>
			</form>
		</div>
	</div>

	
					<form action="Snacky-Payment.php" method ="POST">
				
						<div>
							
							<input type="submit" name="submit" id="submitButton" class="btn btn-success btn-lg btn-block" value="VERIFICA E PAGA" style="display:block;margin-left:auto;margin-right:auto;"/>
					
								<?php
								if(isset($_POST['success'])){
								}
								if(isset($_POST['success'])){
									
									echo $_POST['success'];
								}
								
								
								
								?>
								
						</div>
						
						
						<div class="col-xs-12">
							<input type="sub" name="sub" id="submit" class="btn btn-success btn-lg btn-block" value="PAGA ALLA CONSEGNA" />
							<?php
							
							if(isset($_POST['CONSEGNA'])){
							
							}	
							
							if(isset($_POST['CONSEGNA'])){
								
									echo $_POST['CONSEGNA'];
							}
								
								
								
							?>
						</div>
						
						
						
						
						
						
					</form>

	</div>            

		

		
		
	  </div>
			  
	</section>

	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		 <!-- NAVBAR  -->
		 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
		<li class="nav-item">
				<div class="dropdown show">
				<a class="nav-link text-white decorated-linkText-nav" href="Cart.php" title="Prodotti nel carrello">
				<button  type="button" class="btn btn-light cart btn btn-danger" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <!-- In formaction devo specificare URL dove andare. Ho usato button per metter l'Img-->
					<i id ="icon" class="material-icons Responsive">announcement</i>
					<div id = "space" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
			

  </div>
				</div>
		
				</button> 
				
				
				</a>
				
				
				</li>
			 
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Cart.php" title="Prodotti nel carrello">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">local_grocery_store</i>
					</button>
					
				</a>
				</li>
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Home.php" title="Vai alla Home">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">account_box</i>
					</button>
					
				</a>
				</li>
		
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Logout.php" title="Effettua il Logout">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">exit_to_app</i>
					</button>
					
				</a>
				</li>
		
				
			</ul>
		 </div>
	</nav>

	
	
	

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
