-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 27, 2019 alle 17:43
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snackydb`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts`
--

CREATE TABLE `accounts` (
  `idAccount` int(11) NOT NULL,
  `Abilitato` tinyint(1) NOT NULL DEFAULT '0',
  `cellulare` int(64) NOT NULL,
  `email` varchar(40) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `cognome` varchar(40) NOT NULL,
  `nomeNegozio` varchar(64) DEFAULT NULL,
  `tipologia` varchar(32) NOT NULL,
  `salt` char(128) NOT NULL,
  `chiaveAttivazione` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `accounts`
--

INSERT INTO `accounts` (`idAccount`, `Abilitato`, `cellulare`, `email`, `username`, `password`, `nome`, `cognome`, `nomeNegozio`, `tipologia`, `salt`, `chiaveAttivazione`) VALUES
(1, 0, 111111111, 'email@email.it', 'username', '41e367129fc4045279a971413c026aaf285eb48dee08fdc1d332ced84140d7db4948fbe4c676c562d3e52e83d7948f46d807ce538d1ce11ac863e560a408e97e', 'luca', 'luca', NULL, '0', 'dsa342njkfds7fdsah4322', NULL),
(8, 0, 1234567321, 'miamail@miamail.it', 'ciao', 'db8a2ea3f7878504749cd1f512c35e4f1e0b3c63d3f2ec51b757edd231a2ff02f7b69c54ff06b75c856bc2ba8ed71ce634ca4d08f7cbf74db3885098366ad4c3', 'asd', 'asd', '', 'Cliente', 'ea6657d85d03b1792a87995c50c2f26dd8957cde90a7b387a55c25023a9867f8b3c7dcf1245243de95be59ad253e53587e0fce2641097b09c23f6403b74f4b7e', NULL),
(9, 0, 1234567333, 'mai@asms.com', 'shopshop', 'bfe5542b2a9b0599e5e2b3f3180f739dac16dd8f1ca55e7529fc920c6b3186b0c002db0bbd88cc4297a608d3d7dc92b2db8fe1e2434bed8a1e5405ef34a11ea8', 'negozio', 'shop', 'McEat', 'Fornitore', '2346462131a313a32a26cc4a1dcb9ce33ddf0f7a081ed7599fecb841c8a40a8e474bf5690293efac0a5d11d7793aef65b566a8a1118aa4428ad284b84f7e3604', NULL),
(10, 0, 1234567444, 'mail@email.com', 'usernameusername', '24e715b07ac6f3a16c29ed1c9498c036efa5c1c914cfc1e02ae2df7acc95af6a4494e94435f153d1f1298eb0515da538ffaad12aa498140121d03394f0d78a98', 'luca', 'luca', '', 'Fattorino', '64af468d74b547054dc89903d6290cddb29a2c3faaa0b162829ea3bd2ea1ebe79bed5f39a06a0462b8f28105d8b24c27b518a9b2462d4e002fbafb96748a370d', NULL),
(12, 1, 2147483647, 'leniroth@hotmail.it', 'prova123', '07fe9de3a11d5afe4fcb84d7d7a836489210d692b5ddf6db5c4626c400a03c5af5916a71b56c38459b5078ae2b0ee6bb60b1c076254cbddec4508b184d7caf3b', 'prova', 'prova', '', 'Cliente', '1de2484163668690ed89b3e92d3bc62d7d022e4c3ddca9c6362ae3d2cfdb1e8445a17cdcdd6e7e41e1d6202eb8a07a7db9166bf721799cce4d1d5204a2554554', '64223ccf70bbb65a3a4aceac37e21016'),
(13, 1, 2147483647, 'naraku39@gmail.com', 'lucaluca', '3bbc5ae394aec5c177f152063592f830f4aadc468b27e69dd394a556ae8ee921f2a9631d4c1062ef76b6aed8e5fe189496cc1dc1327e4b584ef37e8b2aad9909', 'luca', 'rispoli', 'justit', 'Fornitore', '5ff4de5a1ade7b2e179b1781a48e302db8e6c3cea68afcc197d5d75ce99caa08a034bc77766c8a987225cdac3793226d8844fe5f4b868a0d884632bbbaa3ece1', 'ab233b682ec355648e7891e66c54191b'),
(14, 1, 1231233211, 'lucarossi@rossi.it', 'lucarossi', 'eda5261392d8e085769aa6c71c0909319e0f6cb8d168d1509e6f82a71c1557f9bfeceec95bad0082a954115a5115c354bfa79f1748f72c4e014a0b19e1bf6b7b', 'Luca', 'rossi', 'RossiShop', 'Fornitore', '730784fa02bb302521cd93958cc9a9fc5547c1bdc82296c25a6a48a87fce99efc253ce64e9f647e9b7c39cb1d6452740f9e1bbe7122e99fb2cd2ba83e9c97e77', 'b6d767d2f8ed5d21a44b0e5886680cb9'),
(15, 0, 1231231231, 'dsadsa@fdsfaf.it', 'prova', '5e574799e72709fbfe2704b764889ffaf1b9d69734e1034a591be3f9e146503643feb3d04576ba37bdefb4109a6ff4f13315c9149a516ddce1faee903c7ece11', 'prova', 'prova', '', 'Cliente', '002c89a9f9262b1afc537750b2b0bd4062e90a143120ed8a9b9e50a9972e59b201cd797013e779ad33095f77e9b54148360e22c82e3bfbd09295cc055be32a67', '4e4b5fbbbb602b6d35bea8460aa8f8e5'),
(16, 1, 1231233215, 'asdasd@asdasd.it', 'asdasdasd', 'f04ee3259d0037a54e044987588abca53224823614af366f70258f2d7a14e79202abcc30b9318848d010d55959eb261422bccf6446538cbd7fcd72558d77d3cb', 'asdasd', 'asdasd', '', 'Cliente', '5d1b5d580d80f6e0d5c3adf908aef137ec594901825f247505d7a7b5ded558d44e6724bc74bbfa6dc8beb727faa160d4e34ff85dfad0af10df937eed1565019e', 'f85454e8279be180185cac7d243c5eb3'),
(17, 1, 1231233212, 'fornitore@fornitore.it', 'fornitore', '0d17e399cb86b7a93af2877fdaf0f48929a2edf2ac9275bba91628521786d40b1a13e75f4694844617f01e33547c2aaa6e07b0f22166edd7f2f790550b7262da', 'fornitore', 'fornitore', 'McDonald', 'Fornitore', 'ef92ebca2e2c788830f0dc2716cb427aacfe852713385af2c27117fd37fbc4cb3df1c04af3f6b11719cad857f574158a0dd85a6f0e1336c2c516585ebca09bbc', '86b122d4358357d834a87ce618a55de0'),
(18, 1, 1234567896, 'fattorino@fattorino.it', 'fattorino', 'f87f49f71e034d2cb2a12996db586feda99a101e8964f2e665737cc41fd9a3474bd3d0eead4725a3c9e9217088b21ca640122eca5bc357019edf6111ac0e8e4c', 'fattorino', 'fattorino', '', 'Fattorino', '24446ccaf2fef8abee2e332d4091cd875a4f13c76b897edeac75d0b8b88a0ef80e68935b101f1c5079336f7fe6efe96d139b3e49097035ac20f5d0d6fcc294e0', 'ea5d2f1c4608232e07d3aa3d998e5135'),
(19, 1, 1234567832, 'amministratore@amministratore.it', 'amministratore', '1a1046384f82c738c60f7dd5c25e4dbf2ec420999243d4cbe996b1c52d067a34253b38b4d1fc8880d30cca2a8bec76876b294b48e1f55ba6962922bae3137853', 'amministratore', 'amministratore', '', 'Amministratore', 'a34e4a020b4afc8b6a4aea76e623e69c02b65481493fd0570bdda35f8049257f1918227009408897712afff86fcb22c34d695375d27762f163c195e4cdee57aa', '362e80d4df43b03ae6d3f8540cd63626');

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello_prodotti`
--

CREATE TABLE `carrello_prodotti` (
  `quantita` int(64) NOT NULL,
  `nomeProdotto` varchar(64) NOT NULL,
  `prezzoUnitario` int(64) NOT NULL,
  `idProdotto` int(32) NOT NULL,
  `idAccount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `carrello_prodotti`
--

INSERT INTO `carrello_prodotti` (`quantita`, `nomeProdotto`, `prezzoUnitario`, `idProdotto`, `idAccount`) VALUES
(1, 'CRISPY McBACON', 4, 171, 16),
(1, 'CRISPY McBACON', 4, 171, 16);

-- --------------------------------------------------------

--
-- Struttura della tabella `consegne`
--

CREATE TABLE `consegne` (
  `idAccount` int(64) NOT NULL,
  `luogoConsegna` varchar(64) NOT NULL,
  `orarioConsegna` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(1, '1545822562'),
(9, '1545853885'),
(13, '1547896108'),
(12, '1547899766'),
(12, '1547899772'),
(12, '1547899779'),
(13, '1547899797'),
(12, '1547899825'),
(12, '1547899832'),
(16, '1547899870'),
(16, '1548341237'),
(16, '1548341621'),
(16, '1548341626'),
(16, '1548345447');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `idNegozio` int(32) NOT NULL,
  `idFattorino` int(32) DEFAULT NULL,
  `destinazione` varchar(64) NOT NULL,
  `statoOrdine` varchar(64) NOT NULL,
  `data` date NOT NULL,
  `statoPagamento` char(1) NOT NULL,
  `metodoPagamento` varchar(64) NOT NULL,
  `costoTotale` int(64) NOT NULL,
  `idOrdine` int(40) NOT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `orarioSpedizione` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`idNegozio`, `idFattorino`, `destinazione`, `statoOrdine`, `data`, `statoPagamento`, `metodoPagamento`, `costoTotale`, `idOrdine`, `idCliente`, `orarioSpedizione`) VALUES
(9, 18, 'Aula Ristoro', 'Completato', '2018-12-29', '0', 'contanti', 33, 3, 16, NULL),
(14, 18, 'dsa', 'Completato', '2019-01-01', 'd', 'dsa', 32, 32, 15, '13:29:22'),
(13, NULL, 'ristoro', 'IN_Spedizione', '2019-01-15', '1', 'Carta', 34, 33, 16, NULL),
(17, 18, 'Aula Ristoro', 'Completato', '2019-01-17', '1', 'Contanti', 17, 34, 16, '12:06:00'),
(17, 18, 'Entrata Primo Piano', 'Completato', '2019-01-24', '1', 'Contanti', 26, 35, 16, '12:06:00'),
(16, NULL, 'Aula', 'Accettato', '2019-01-24', '1', 'bancomat', 96, 36, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-24', '1', 'bancomat', 96, 37, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-24', '1', 'bancomat', 96, 38, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-24', '1', 'bancomat', 96, 39, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-25', '1', 'bancomat', 96, 40, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-25', '1', 'bancomat', 96, 41, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-25', '1', 'bancomat', 96, 42, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-25', '1', 'bancomat', 96, 43, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-25', '1', 'bancomat', 96, 44, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-25', '1', 'bancomat', 96, 45, 16, NULL),
(16, NULL, 'Aula', 'Accettato', '2019-01-25', '1', 'bancomat', 96, 46, 16, NULL),
(17, NULL, 'Ingresso piano terra', 'Accettato', '2019-01-26', '1', 'Bancomat', 103, 47, 16, '10:30:00'),
(13, NULL, 'Punto Ristoro', 'Accettato', '2019-01-26', '1', 'Bancomat', 184, 48, 16, '18:00:00'),
(14, NULL, 'Punto Ristoro', 'Accettato', '2019-01-26', '1', 'Bancomat', 184, 49, 16, '18:00:00'),
(13, NULL, 'Ingresso primo piano', 'Accettato', '2019-01-27', '0', 'Contanti', 116, 50, 16, '14:30:00'),
(17, NULL, 'Ingresso primo piano', 'Accettato', '2019-01-27', '0', 'Contanti', 116, 51, 16, '14:30:00'),
(17, NULL, 'Ingresso primo piano', 'Accettato', '2019-01-27', '1', 'Bancomat', 107, 52, 16, '13:00:00'),
(17, NULL, 'Ingresso piano terra', 'Accettato', '2019-01-27', '0', 'Contanti', 100, 53, 16, '09:30:00'),
(17, NULL, 'Ingresso piano terra', 'Accettato', '2019-01-27', '0', 'Contanti', 4, 54, 16, '17:30:00'),
(17, NULL, 'Ingresso primo piano', 'Accettato', '2019-01-27', '0', 'Contanti', 4, 55, 16, '18:00:00'),
(17, NULL, 'Ingresso primo piano', 'Accettato', '2019-01-27', '0', 'Contanti', 8, 56, 16, '09:00:00'),
(17, NULL, 'Ingresso piano terra', 'Accettato', '2019-01-27', '0', 'Contanti', 20, 57, 16, '09:00:00'),
(14, NULL, 'Ingresso piano terra', 'Accettato', '2019-01-27', '1', 'Bancomat', 52, 58, 16, '09:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_ordini`
--

CREATE TABLE `prodotti_ordini` (
  `idProdotto` int(32) NOT NULL,
  `idOrdine` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `tipologia` varchar(64) DEFAULT NULL,
  `idProdotto` int(64) NOT NULL,
  `nomeProdotto` varchar(64) NOT NULL,
  `prezzoUnitario` int(64) NOT NULL,
  `idAccount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`tipologia`, `idProdotto`, `nomeProdotto`, `prezzoUnitario`, `idAccount`) VALUES
('DESSERT', 100, 'CALZONE FRITTO ALLA NUTELLA', 4, 13),
('BEVANDA', 101, 'ACQUA NATURALE', 1, 13),
('BEVANDA', 102, 'COCACOLA', 2, 13),
('BEVANDA', 103, 'CAFFE\' AROMA NOCCIOLA', 2, 13),
('PIADINA', 130, 'PIADINA COTTO-MOZZARELLA', 4, 13),
('PIADINA', 131, 'PIADINA POMODORO-MOZZARELLA', 3, 13),
('PIADINA', 132, 'PIADINA SALSICCIA-PATATE-MOZZARELLA', 5, 13),
('PIADINA', 133, 'PIADINA CRUDO-RUCOLA-PARMIGIANO', 3, 13),
('PIADINA', 134, 'PIADINA SALAME PICCANTE-STRACCHINO', 4, 13),
('PIADINA', 135, 'PIADINA COTTO-FONTINA', 4, 13),
('PIADINA', 136, 'CRESCIONE COTTO-MOZZARELLA', 5, 13),
('PIADINA ', 137, 'CRESCIONE FUNGHI-SALSICCIA-MOZZARELLA', 4, 13),
('PIADINA ', 138, 'CRESCIONE VERDURE MISTE', 4, 13),
('DESSERT', 139, 'PIADINA NUTELLA', 3, 13),
('PIZZA', 140, 'PIZZA MARGHERITA', 4, 14),
('PIZZA', 141, 'PIZZA DIAVOLA', 5, 14),
('PIZZA', 142, 'PIZZA CAPRICCIOSA', 5, 14),
('PIZZA', 143, 'PIZZA SALSICCIA E FUNGHI', 6, 14),
('PIZZA', 144, 'PIZZA PROSCIUTTO COTTO ROSSA', 6, 14),
('PIZZA', 145, 'PIZZA TARTUFO E PROVOLA (BIANCA)', 7, 14),
('PIZZA', 146, 'PIZZA BUFALA ROSSA', 7, 14),
('PIZZA', 147, 'PIZZA CARCIOFI E PROSCIUTTO', 5, 14),
('PIZZA', 148, 'PIZZA CRUDO RUCOLA PARMIGIANO', 8, 14),
('PIZZA', 149, 'PIZZA PATATE E WURSTEL ROSSA', 7, 14),
('PIZZA', 150, 'PIZZA PEPERONI E SALSICCIA', 7, 14),
('DESSERT', 151, 'TIRAMISU\'', 3, 14),
('DESSERT', 152, 'TARTUFO AL CIOCCOLATO', 3, 14),
('DESSERT', 153, 'TORTA DI MELE ', 3, 14),
('DESSERT', 154, 'PANNA COTTA', 2, 14),
('DESSERT', 155, 'CHEESECAKE FRUTTI DI BOSCO', 3, 14),
('BEVANDA', 156, 'ACQUA NATURALE', 1, 14),
('BEVANDA', 157, 'ACQUA FRIZZANTE', 1, 14),
('BEVANDA', 158, 'COCACOLA 0,3LT', 2, 14),
('BEVANDA', 159, 'FANTA', 1, 14),
('HAMBURGER', 171, 'CRISPY McBACON', 4, 17),
('HAMBURGER', 172, 'BIG MAC', 3, 17),
('HAMBURGER', 173, 'CHICKEN BBQ', 4, 17),
('HAMBURGER', 174, 'McCHICKEN', 3, 17),
('HAMBURGER', 175, 'GLUTEN FREE BURGER', 5, 17),
('HAMBURGER', 176, 'FILET-O-FISH BURGER', 6, 17),
('SANDWICH', 177, 'McTOAST', 2, 17),
('SANDWICH', 178, 'CHICKEN COUNTRY', 4, 17),
('PIADINA', 179, 'McWRAP CESAR CHEESE', 5, 17),
('PIADINA', 180, 'CLASSIC WRAP', 3, 17),
('DESSERT', 181, 'MILKSHAKE - CHOCOLATE', 1, 17),
('DESSERT', 182, 'MILKSHAKE - VANIGLIA', 1, 17),
('DESSERT', 183, 'MILKSHAKE - BANANA', 1, 17),
('DESSERT', 184, 'McFLURRY - SMARTIES', 2, 17),
('DESSERT', 185, 'SUNDAE FRUITS', 1, 17),
('BEVANDA', 186, 'ACQUA NATURALE', 1, 17),
('BEVANDA', 187, 'ACQUA FRIZZANTE', 1, 17),
('BEVANDA', 188, 'COCACOLA', 2, 17),
('BEVANDA', 189, 'FANTA', 2, 17),
('BEVANDA', 190, 'ESTATHE\'', 2, 17),
('BEVANDA', 191, 'BIRRA HEINEKEN', 3, 17);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`idAccount`),
  ADD UNIQUE KEY `IDCLIENTE_1` (`email`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`idOrdine`),
  ADD KEY `idNegozio` (`idNegozio`),
  ADD KEY `idFattorino` (`idFattorino`),
  ADD KEY `idCliente` (`idCliente`);

--
-- Indici per le tabelle `prodotti_ordini`
--
ALTER TABLE `prodotti_ordini`
  ADD PRIMARY KEY (`idProdotto`,`idOrdine`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`idProdotto`),
  ADD KEY `idAccount` (`idAccount`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `accounts`
--
ALTER TABLE `accounts`
  MODIFY `idAccount` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `idOrdine` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  MODIFY `idProdotto` int(64) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `idCliente` FOREIGN KEY (`idCliente`) REFERENCES `accounts` (`idAccount`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `idFattorino` FOREIGN KEY (`idFattorino`) REFERENCES `accounts` (`idAccount`),
  ADD CONSTRAINT `idNegozio` FOREIGN KEY (`idNegozio`) REFERENCES `accounts` (`idAccount`);

--
-- Limiti per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  ADD CONSTRAINT `idAccount` FOREIGN KEY (`idAccount`) REFERENCES `accounts` (`idAccount`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
