<?php
include 'php/db_connect.php';
include 'php/login_utilities.php';

sec_session_start(); 

header('Content-Type: application/json');

$id = 1;
if(isset($_GET["request"])){
	switch ($_GET["request"]) {
		//ritorna tutti gli ordini in attesa di un fattorino che spedisca ( PER FATTORINI)
		case "todelivery":
			$stmt = $mysqli->prepare("SELECT * FROM ordine WHERE idFattorino is NULL ");
			$stmt->execute();
			$result = $stmt->get_result();
			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}
			$stmt->close();
			print json_encode($output);
			break;
			//Ritorna tutti gli item all	'interno del carrello (PER CLIENTI)
			case "tobuy":
			$stmt = $mysqli->prepare("SELECT * FROM carrello_prodotti WHERE idAccount = ? ");
			$stmt->bind_param('i', $_SESSION['user_id']);
			$stmt->execute();
			$result = $stmt->get_result();
			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}
			$stmt->close();
			print json_encode($output);
			break;
		// Ritorna tutti gli ordini associati all'account che sono in spedizione (PER CLIENTI)
		case "toWait":
			$stmt = $mysqli->prepare("SELECT * FROM ordine WHERE idCliente = ? AND statoOrdine = 'in_spedizione'");
			$stmt->bind_param('i', $_SESSION['user_id']);
			$stmt->execute();
			$result = $stmt->get_result();
			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}
			$stmt->close();
			print json_encode($output);
			break;
		
		// Ritorna tutti gli ordini associati all'account che non sono stati completati (PER FORNITORI)
		case "toManage":
			$stmt = $mysqli->prepare("SELECT * FROM ordine WHERE idNegozio = ? AND statoOrdine != 'completato'");
			$stmt->bind_param('i', $_SESSION['user_id']);
			$stmt->execute();
			$result = $stmt->get_result();
			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}
			$stmt->close();
			print json_encode($output);
			break;
			
		// Ritorna il numero di ordini associati all'account che non sono ancora stati completati (PER FORNITORI)
		case "numOrdersToManage":
			$stmt = $mysqli->prepare("SELECT COUNT(*) AS num_orders_to_manage FROM ordine WHERE idNegozio = ? AND statoOrdine != 'completato'");
			$stmt->bind_param('i', $_SESSION['user_id']);
			$stmt->execute();
			$stmt->bind_result($num_orders);	
			$stmt->fetch();
			$output = array("num_orders" => $num_orders);
			print json_encode($output);
			break;
			
			
		// Ritorna il numero di ordini associati all'account che sono in spedizione (PER CLIENTI)
		case "numOrdersToWait":
			$stmt = $mysqli->prepare("SELECT COUNT(*) AS num_orders_to_wait FROM ordine WHERE idCliente = ? AND statoOrdine = 'in_spedizione'");
			$stmt->bind_param('i', $_SESSION['user_id']);
			$stmt->execute();
			$stmt->bind_result($num_orders);	
			$stmt->fetch();
			$output = array("num_orders" => $num_orders);
			print json_encode($output);
			break;
	
		// Ritorna il numero di ordini in attesa di un fattorino (PER FATTORINI)
		case "numOrdersToDeliver":
			$stmt = $mysqli->prepare("SELECT COUNT(*) AS num_orders_to_deliver FROM ordine WHERE idFattorino is NULL");
			$stmt->execute();
			$stmt->bind_result($num_orders);	
			$stmt->fetch();
			$output = array("num_orders" => $num_orders);
			print json_encode($output);
			break;
			
		case "numOrdersToBuy":
			$stmt = $mysqli->prepare("SELECT COUNT(*) AS num_orders_to_buy FROM carrello_prodotti WHERE idAccount = ?");
			$stmt->bind_param('i', $_SESSION['user_id']);
			$stmt->execute();
			$stmt->bind_result($num_orders);	
			$stmt->fetch();
			$output = array("num_orders" => $num_orders);
			print json_encode($output);
			break;
	}
	
	
}
	


?>