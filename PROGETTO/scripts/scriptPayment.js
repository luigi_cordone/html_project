
$(document).ready(function(){

	
	$("#submit").click(function(){
		
		let form = document.createElement('form');
			  form.action = 'Snacky-Payment.php';
			  form.method='POST';
			  
			  form.innerHTML = '<input name="CONSEGNA" value="">';
			  
			  document.body.append(form);
			  
			  form.submit();
	});
	
	$("#submitButton").click(function(){
		$("div.alert-js").hide();
		console.log("dsa00");
		$("#phpAlert").hide();
		event.preventDefault();
		errors = "";
		
		var numero = $("input#inputNumber").val();
		var date = $("input#inputDate").val();
		var code = $("input#inputCode").val();
		
		if(numero == null  || numero.length < 15 || numero.length > 16){
			document.getElementById("inputNumber").focus();
			document.getElementById('inputNumber').style.borderColor = "red";
			errors += "Perfavore, inserisci il numero della carta <br/>"; 
		}else{
			document.getElementById('inputNumber').style.borderColor = "";
		}
		
		if(date == null  || date.length < 5 ){
			document.getElementById("inputDate").focus();
			document.getElementById('inputDate').style.borderColor = "red";
			errors += "Perfavore, inserisci la data in modo corretto <br/>";
		}else{
			document.getElementById('inputDate').style.borderColor = "";
		}
		
		if(code == null  || code.length < 3 ){
			document.getElementById("inputCode").focus();
			document.getElementById('inputCode').style.borderColor = "red";
			errors += "Perfavore, inserisci il CVC in modo corretto <br/>";
		}else{
			document.getElementById('inputCode').style.borderColor = "";
		}
		
		if(errors.length > 0)
		{
			var nome = $("div.alert-js p").html(errors);
			$("div.alert-js").show();
		}
		
		else{
			  let form = document.createElement('form');
			  form.action = 'Snacky-Payment.php';
			  form.method='POST';
			  
			  form.innerHTML = '<input name="success" value="">';
			  
			  document.body.append(form);
			  
			  form.submit();
			  
			  //alert("ORDINE EFFETTUATO CON SUCCESSO!");
		}
	});
});