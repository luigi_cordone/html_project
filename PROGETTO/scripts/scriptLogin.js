$(document).ready(function(){
	$("form button").click(function(){
		$("div.alert-js").hide();
		$("#phpAlert").hide();
		event.preventDefault();
		errors = "";
		
		var nome = $("input#inputUsername").val();
		var password = $("input#inputPassword").val();

		if(nome == null  || nome.length < 1){
			document.getElementById("inputUsername").focus();
			document.getElementById('inputUsername').style.borderColor = "red";
			errors += "Perfavore, inserisci il tuo Username <br/>";
		}else{
			document.getElementById('inputUsername').style.borderColor = "";
		}
		

		if(password == null || password .length < 6){
			document.getElementById("inputPassword").focus();
			document.getElementById('inputPassword').style.borderColor = "red";
			errors += "Perfavore, inserisci una password valida <br/>";
		}else{
			document.getElementById('inputPassword').style.borderColor = "";
		}

		if(errors.length > 0)
		{
			var nome = $("div.alert-js p").html(errors);
			$("div.alert-js").show();
		}
		else
		{
			
			var p = document.createElement("input");
			document.getElementById("myForm").appendChild(p);
			p.name = "p";
			p.type = "hidden"
			p.value = hex_sha512(password);
			$("input#inputPassword").value = "";
			console.log("Data Submitted");
			$(this).parent().submit();
			document.getElementById("myForm").submit();
		}

	});
});