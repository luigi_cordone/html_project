function validateEmail(email) 
{
  var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
  return regex.test(email);
}

function validateCell(cell) 
{
  var regex = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
  return regex.test(cell);
}

$(document).ready(function(){
	var Privileges = jQuery('#dropDown');
	var select = this.value;
	Privileges.change(function () {
		if ($(this).val() == 'Fornitore') {
			$('.shopName').show();
		}
		else {
			$('.shopName').hide(); 
		}
});
	
	$("#submitButton").click(function(){
		$("div.alert-js").hide();
		event.preventDefault();
		errors = "";
		
		var nome = $("input#inputName").val();
		var cognome= $("input#inputSurname").val();
		var email = $("input#inputEmail").val();
		var cellphone = $("input#inputCell").val();
		var username = $("input#inputUsername").val();
		var password = $("input#inputPassword").val();
		var passwordConfirm = $("input#inputPasswordTest").val();
		var userType = $("#dropDown").val();
		var shopName = $("input#inputShop").val();

		if(nome == null  || nome.length < 1){
			document.getElementById("inputName").focus();
			document.getElementById('inputName').style.borderColor = "red";
			errors += "Perfavore, inserisci il tuo Nome <br/>";
		}else{
			document.getElementById('inputName').style.borderColor = "";
		}
		
		if(cognome == null  || cognome.length < 1){
			document.getElementById("inputSurname").focus();
			document.getElementById('inputSurname').style.borderColor = "red";
			errors += "Perfavore, inserisci il tuo Cognome <br/>";
		}else{
			document.getElementById('inputSurname').style.borderColor = "";
		}
		
		if(email == null || !validateEmail(email)){
			document.getElementById("inputEmail").focus();
			document.getElementById('inputEmail').style.borderColor = "red";
			errors += "Perfavore, inserisci una Email valida <br/>";
		}else{
			document.getElementById('inputEmail').style.borderColor = "";
		}
		
		if(cellphone == null  || cellphone.length < 10  || !validateCell(cellphone)){
			document.getElementById("inputCell").focus();
			document.getElementById('inputCell').style.borderColor = "red";
			errors += "Perfavore, inserisci un numero di cellulare valido <br/>";
		}else{
			document.getElementById('inputCell').style.borderColor = "";
		}
		
		if(username == null  || username.length < 1){
			document.getElementById("inputUsername").focus();
			document.getElementById('inputUsername').style.borderColor = "red";
			errors += "Perfavore, inserisci il tuo Username <br/>";
		}else{
			document.getElementById('inputUsername').style.borderColor = "";
		}
		
		if(password == null || password .length < 6){
			document.getElementById("inputPassword").focus();
			document.getElementById('inputPassword').style.borderColor = "red";
			errors += "Perfavore, inserisci una password valida <br/>";
		}else{
			document.getElementById('inputPassword').style.borderColor = "";
		}
		
		if(passwordConfirm == null || passwordConfirm != password){
			document.getElementById("inputPasswordTest").focus();
			document.getElementById('inputPasswordTest').style.borderColor = "red";
			document.getElementById('inputPassword').style.borderColor = "red";
			errors += "I due campi password devono coincidere! <br/>";
		}else{
			document.getElementById('inputPasswordTest').style.borderColor = "";
		}
		

		if(userType == null ){
			document.getElementById("dropDown").focus();
			document.getElementById('dropDown').style.borderColor = "red";
			errors += "Perfavore, inserisci una tipologia di utente <br/>";
				
		}else{
			document.getElementById('dropDown').style.borderColor = "";
		}
		
		if((shopName == null  || shopName.length < 1 )&&  userType === "Fornitore"){
			document.getElementById("inputShop").focus();
			document.getElementById('inputShop').style.borderColor = "red";
			errors += "Perfavore, inserisci un nome per il tuo negozio <br/>";
		}else{
			document.getElementById('inputShop').style.borderColor = "";
		}
		
		

		if(errors.length > 0)
		{
			var nome = $("div.alert-js p").html(errors);
			$("div.alert-js").show();
		}
		else
		{
			var p = document.createElement("input");
			document.getElementById("myForm").appendChild(p);
			p.name = "p";
			p.type = "hidden"
			p.value = hex_sha512(password);
			$("input#inputPassword").value = "";
			console.log("Data Submitted");
			$(this).parent().submit();
			document.getElementById("myForm").submit();
		}

	});
});