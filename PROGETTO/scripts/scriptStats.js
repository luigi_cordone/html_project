$(document).ready(function(){
  $.ajax({
    url: "stats.php",
    method: "GET",
    success: function(data) {
      var shop = [];
      var money = [];
		obj = JSON.parse(data);
      for(var i in obj) {
        shop.push(obj[i].nomeNegozio);
        money.push(obj[i].ricavato);

      }
	  makeChart(shop,money);

     
	}

});

 $.ajax({
    url: "ricavi.php",
    method: "GET",
    success: function(data) {
      var date = [];
      var money = [];
		obj = JSON.parse(data);
      for(var i in obj) {
		 
        date.push(obj[i].data);
        money.push(obj[i].ricavoGiornaliero);

      }
	  makeHist(date,money);

     
	}

});
function makeHist(date,money){
	console.log(date[0]);
	new Chart(document.getElementById("line-chart"), {
	type: 'line',
	data: {
    labels: [date[0],date[1],date[2],date[3],date[4]],
    datasets: [{ 
        data: [money[0],money[1],money[2],money[3],money[4]],
        label: "Ricavo Giornaliero",
        borderColor: "#3e95cd",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: ''
    }
  }
});
}
function makeChart(shop,money){
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [shop[0], shop[1], shop[2], shop[3]],
        datasets: [{
            label: 'Ricavato',
            data: [money[0], money[1], money[2], money[3]],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
}
});