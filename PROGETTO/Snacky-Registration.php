<?php

include 'php/db_connect.php';
include 'php/login_utilities.php';
include 'php/utilities.php';
if(isset($_SESSION['tipologia'])){
if($_SESSION['tipologia'] == 'Cliente'){
				header('Location: Snacky-Home.php');
				exit();
			}
}
$name = $email = $isInserted = $shopName = $password = $testPassword = $surname = $type = $username =  "";
$cellphone = 0;
$errors = "";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
  if (empty($_POST["name"])) {
	  
    $errors = $errors ."Il nome è obbligatorio <br/>";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
     $errors = $errors . "Nel campo 'nome' sono ammessi solo lettere e spazi <br/>"; 
    }
  }
  if (empty($_POST["surname"])) {
    $errors = $errors ."Il cognome è obbligatorio <br/>";
  } else {
    $surname = test_input($_POST["surname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$surname)) {
      $errors = $errors ."Nel campo 'cognome' sono ammessi solo lettere e spazi <br/>"; 
    }
  }
  if (empty($_POST["username"])) {
    $errors = $errors ."Il username è obbligatorio <br/>";
  } else {
    $username = test_input($_POST["username"]);
  }
  
   if (empty($_POST["email"])) {
    $errors = $errors ."La email è obbligatoria <br/>";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $errors = $errors ."L'email inserita non è valida <br/>"; 
    }
  }
  
	if (empty($_POST["nCell"])) {
    $errors = $errors . "Il numero di cellulare è obbligatorio <br/>";
  } else {
    $cellphone = test_input($_POST["nCell"]);
    }
	

	
	if (empty($_POST["userType"])) {
   $errors = $errors . "La tipologia di utente è obbligatoria <br/>";
  }  else{
	$type = $_POST["userType"];  // Storing Selected Value In Variable
  }
  
  if (empty($_POST["p"]) || empty($_POST["passwordTest"])) {
   $errors = $errors . "La password e la conferma sono obbligatori <br/>";
  }  else{
	  if(hash('sha512', $_POST["passwordTest"])===$_POST["p"]){
	$password = $_POST["p"];  // Storing Selected Value In Variable
	  }else{
		   $errors = $errors . "I due campi password devono coincidere! <br/>";
	  }
  }
  
  if($type === "Fornitore"){
		 if (empty($_POST["shopName"])){
			 $errors = $errors . "Il nome del negozio è obbligatorio per i fornitori <br/>";
		 } else{
			 $shopName = test_input($_POST["shopName"]);
		 }
	}

if(strlen($errors)==0){
	
	  if ($stmt = $mysqli->prepare("SELECT * FROM accounts WHERE email = ?" )) { 
      $stmt->bind_param('s', $email); 
      // Eseguo la query creata.
      $stmt->execute();
      $stmt->store_result();
      if($stmt->num_rows > 0) {
         $errors = $errors . "L'email è già associata ad un'altro account! <br/>";
      }
	  }
	  if ($stmt = $mysqli->prepare("SELECT * FROM accounts WHERE username = ?" )) { 
      $stmt->bind_param('s', $username); 
      // Eseguo la query creata.
      $stmt->execute();
      $stmt->store_result();
      if($stmt->num_rows > 0) {
         $errors = $errors . "L'username è già associato ad un'altro account! <br/>";
      }
	  }
	  
	  if ($stmt = $mysqli->prepare("SELECT * FROM accounts WHERE cellulare = ?" )) { 
      $stmt->bind_param('i', $cellphone); 
      // Eseguo la query creata.
      $stmt->execute();
      $stmt->store_result();
      if($stmt->num_rows > 0) {
         $errors = $errors . "Il numero di cellulare è già associato ad un'altro account! <br/>";
      }
	  }
	
	if(strlen($errors)==0){
	// Crea una chiave casuale
	$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
	// Crea una password usando la chiave appena creata.
	$password = hash('sha512', $password.$random_salt);
	// Generate activation Key
	$hash = md5( rand(0,1000) );
	
	
	if ($insert_stmt = $mysqli->prepare("INSERT INTO accounts (cellulare,email,username,password,nome,cognome,tipologia,salt,nomeNegozio, chiaveAttivazione) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {    
		$insert_stmt->bind_param('isssssssss', $cellphone, $email, $username, $password, $name, $surname, $type, $random_salt,$shopName, $hash); 
		// Esegui la query ottenuta.
		$isInserted = $insert_stmt->execute();
		
		if(!$isInserted){
			$errors = $insert_stmt->error;
		}
		else{
			//L'account è stato creato
			
			send_activation($email,$hash);
			header('Location: registration-success.php');
			exit();
		}
	}
		
}
}
	
}
	  

 ?>



<!DOCTYPE html>
<html lang="it">
<head>
	<title>Snacky-Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="scripts/sha512.js"></script>
	<script src="scripts/scriptRegistration.js"></script>
	<link rel="stylesheet" type="text/css" href="css/	registration.css">		
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>


	<!-- Sezione di Login -->
	
<section class="login-block">
    <div class="container">
	<div class="row">
<!-- Banner titolo-->
		<div class="col-md-12		 login-sec">	
		<h2 class="text-center">Registrazione</h2>
		
		<?php
			if(strlen($errors)!=0){
				
				?>
		<div id = "phpAlert" class="alert alert-danger alert-php" role="alert"  style="margin-top:5%">
					Errore durante la registrazione!
					<p><?=$errors?></p>
				</div>
			<?php
			}
			?>
		<?php
			if(strlen($errors)==0 && $isInserted){
				
				?>
		<div id = "phpSuccess" class="alert alert-success" role="alert"  style="margin-top:5%">
					Registrazione effettuata con Successo, riceverai a breve una email con le istruzioni necessarie per attivare il tuo account!
					<p><?=$errors?></p>
				</div>
			<?php
			}
			?>
		<div class="alert alert-danger alert-js" role="alert" style="display: None">
					Dati inseriti non corretti
					<p></p>
				</div>
		</div>
<!-- Colonna di sinistra -->

		<div class="col-md-4	 login-sec">	
		<form id = "myForm" class="login-form" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		   <div class="form-group">
			<label for="inputName" class="text-uppercase">Nome</label>
			<input type="text" id="inputName" placeholder="Il tuo nome" name="name" class="form-control" placeholder="">
		</div>
		
			<div class="form-group">
			<label for="inputSurname" class="text-uppercase">Cognome</label>
			<input type="text" id="inputSurname" placeholder="Il tuo cognome" name="surname" class="form-control" placeholder="">
		</div>
		<div class="form-group">
    <label for="inputUsername" class="text-uppercase">Username</label>
    <input type="text" id="inputUsername" placeholder="Il tuo username" name="username" class="form-control" placeholder="">
 
  </div>
		    
	
	
		</div>
<!-- Colonna centrale -->
	<div class="col-md-4	 login-sec">
		  <div class="form-group">
    <label for="inputPassword" class="text-uppercase">Password</label>
    <input type="password" id="inputPassword" placeholder="Almeno 6 Caratteri" name="password" class="form-control" placeholder="">
  </div>
		  <div class="form-group">
    <label for="inputPasswordTest" class="text-uppercase">Conferma Password</label>
    <input type="password" id="inputPasswordTest" placeholder="Conferma la tua password" name="passwordTest" class="form-control" placeholder="">
  </div>
  
  		<div class="form-group">
			<label for="inputEmail" class="text-uppercase">Email	</label>
			<input type="text" id="inputEmail" placeholder="Inserisci una email valida" name="email" class="form-control" placeholder="">
		</div>
	</div>
	
<!-- Colonna di destra-->
	<div class="col-md-4	 login-sec">
	
	
		<div class="form-group">
			<label for="inputCell" class="text-uppercase">N° Cellulare</label>
			<input type="tel" id="inputCell" placeholder="Inserisci un numero di cellulare valido" name="nCell" class="form-control" placeholder="">
		</div>
	<label for="dropDown" class="text-uppercase">Tipo Utente:</label>
	<div class="form-group">
		<select name="userType" class="form-control" id="dropDown">
			<option disabled selected value> Scegli un tipo </option>
			<option>Cliente</option>
			<option>Fornitore</option>
			<option>Fattorino</option>
    </select>
  </div>
 		<div class="form-group shopName" style="display: None">
			<label for="inputShop" class="text-uppercase">Nome del Negozio:	</label>
			<input type="text" id="inputShop" placeholder="Il nome del tuo negozio" name="shopName" class="form-control" placeholder="">
		</div>
</div>

	
	
	</div>
	<!-- Riga inferiore del form -->	
	<div class="col-md-12	 login-sec">
	<div class="text-center"> 
		<div class="form-group">
		 <button id="submitButton" class="btn btn-login">Registrati subito!</button>
		</div>
				

		</div>
	</form>
		
		
</div>
</div>
</section>
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Registration.php" title="Registrati">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">group_add</i>
					</button>
					
				</a>
				</li>
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Login.php" title="Effettua il login">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">input</i>
					</button>
					
				</a>
				</li>
			
		
			
			</ul>
		 </div>
	</nav>
	
	

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>
<!--SCRAP.ME CODE--><script type="text/javascript">var scrapmekey = "874331A05194A42FA224EF58B70AD866";</script><script type="text/javascript" src="https://scrap.me/widget/popup/v1/script.js"></script><!--SCRAP.ME CODE-->


</body>
</html>
