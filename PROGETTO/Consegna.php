<?php
include 'php/login_utilities.php';
include 'php/utilities.php';
$errors = "";
$sommaTot=0;

//ORA E DATA CORRENTE DELL'ORDINE
$now = new DateTime();
$date=$now->format('Y-m-d');    
$time=$now->format('H:i:s');

sec_session_start()	;

//echo $_SESSION['ora']; 
//echo $_SESSION['luogo']; 

$ora=$_SESSION['ora'];
$luogo=$_SESSION['luogo'];
//echo $ora;
//echo $luogo;
?>



<!DOCTYPE html>
<html lang="it">
<head>
	<title>Consegna</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/verify.css">	
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<script src="scripts/notify.min.js"></script>
	<script src="scripts/scriptNotifyClienti.js"></script>
	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>


	<!-- per inserire dentro il riquadro bianco,  inserire gli elementi sotto il div row-->
<section class="login-block">
    <div class="container">
	<div class="row">
	<div class="col-md-12		 login-sec">	
	
		<div id = "phpSuccess" class="alert alert-success" role="alert"  style="margin-top:5%">
					ORDINE EFFETTUATO CON SUCCESSO! PROVVEDEREMO ALLA CONSEGNA DEL SUO ORDINE!
					
					<?php
													
						$servername="localhost";
						$username ="root";
						$password ="";
						$database = "snackydb";
						
						$sommaTot=0;
						$conn = new mysqli($servername, $username, $password, $database);
						
							$result = $conn->query("SELECT quantita, nomeProdotto, prezzoUnitario FROM carrello_prodotti");
							while($row = $result->fetch_assoc() ) {
								
								$totale = $row["quantita"] * $row["prezzoUnitario"];
								$sommaTot= $sommaTot + $totale; 
								
							}
							
							$stato = "Accettato";
							$metodo = "Contanti";
							$statoPag = 0;
							$fattorino = null;
							$posto=$luogo;
							$orario=$ora;
							//echo $posto;
							//echo $orario;
							
							
							if($stmt= $conn->prepare("SELECT DISTINCT p.idAccount FROM prodotto p JOIN carrello_prodotti c ON c.idProdotto=p.idProdotto WHERE c.idAccount=$_SESSION[user_id]"));
							$stmt->execute();
							$res=$stmt->get_result();
							$availableProviders=array();

							while($row = $res->fetch_assoc()){
								array_push($availableProviders, $row["idAccount"]);
							}

							foreach($availableProviders as $val){
								if ($insert_stmt = $conn->prepare("INSERT INTO ordine (idNegozio, idFattorino, destinazione, statoOrdine, data, statoPagamento, 
									metodoPagamento, costoTotale, idCliente, orarioSpedizione) VALUES(?,?,?,?,?,?,?,?,?,?)")){
									$insert_stmt->bind_param("iisssisiis" , $val, $fattorino ,$posto,$stato,$date,$statoPag ,$metodo,$sommaTot, $_SESSION['user_id'],$orario);
									$isInserted = $insert_stmt->execute();
									if ($stmt = $conn->prepare("SELECT email FROM accounts WHERE idAccount = ? LIMIT 1")) { 
										$stmt->bind_param('i', $val); 
										$stmt->execute(); 
										$stmt->store_result();
										$stmt->bind_result($email);
										$stmt->fetch();
	
									if($stmt->num_rows == 1) { 
										send_order_to_shop($email);
									}
									if($stmt= $conn->prepare("SELECT email FROM accounts WHERE tipologia='Fattorino'"));
							$stmt->execute();
							$res=$stmt->get_result();
							$availableShippers=array();

							while($row = $res->fetch_assoc()){
								array_push($availableShippers, $row["email"]);
							}
									foreach($availableShippers as $email){
										send_order_to_delivery($email);
									}
	
									if(!$isInserted){
										$errors = $insert_stmt->error;
										echo $errors;
									}
									else{
										
									}
								}
							}
							}
							
							$sql= "DELETE FROM carrello_prodotti WHERE idAccount=".$_SESSION['user_id'];
																
							if ($conn->query($sql) === TRUE) {
								echo "";
							} else {
								echo "";
							}
						
					$conn->close();
				
					?>
					
		</div>
				<a id="loginButton" href="Snacky-Home.php" class="btn btn-default">Torna alla schermata Home</a>
	</div>
    </div>
    </div>
</section>

	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
		<li class="nav-item">
				<div class="dropdown show">
				<a class="nav-link text-white decorated-linkText-nav" href="Cart.php" title="Prodotti nel carrello">
				<button  type="button" class="btn btn-light cart btn btn-danger" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <!-- In formaction devo specificare URL dove andare. Ho usato button per metter l'Img-->
					<i id ="icon" class="material-icons Responsive">announcement</i>
					<div id = "space" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
			

  </div>
				</div>
		
				</button> 
				
				
				</a>
				
				
				</li>
			 
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Cart.php" title="Prodotti nel carrello">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">local_grocery_store</i>
					</button>
					
				</a>
				</li>
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Home.php" title="Vai alla Home">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">account_box</i>
					</button>
					
				</a>
				</li>
		
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Logout.php" title="Effettua il Logout">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">exit_to_app</i>
					</button>
					
				</a>
				</li>
		
				
			</ul>
		 </div>
	</nav>

	
	

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
