<?php

function send_activation($destinatario,$hash){
	
	$to      = $destinatario; 
	$subject = 'Benvenuto in Snacky ! ';
	$message = "
 
	Grazie per esserti Registrato a Snacky!
	Il tuo account e' stato creato, per attivarlo, clicca sul link sottostante.
 
	------------------------
	http://localhost/html_project/PROGETTO/verify.php?hash=$hash

	";

	$headers = 'From:SnackyService@gmail.com' . "\r\n"; 
	mail($destinatario, $subject, $message, $headers);

}

function send_order_to_delivery($destinatario){
	$to      = $destinatario; 
	$subject = 'Un cliente ha effettuato un ordine ! ';
	$message = "
 
	Un nuovo ordine e' disponibile! 
	Per accettare la consegna o per avere ulteriori informazioni, clicca sul link sottostante.
 
	------------------------
	http://localhost/html_project/PROGETTO/Fattorini-Ordini-Vacanti.php

	";

	$headers = 'From:SnackyService@gmail.com' . "\r\n"; 
	mail($destinatario, $subject, $message, $headers);
	
	
}

function send_order_to_shop($destinatario){
	$to      = $destinatario; 
	$subject = 'Un cliente ha effettuato un ordine! ';
	$message = "
 
	Complimenti, Un cliente ha effettuato un ordine!
	Abbiamo gia' provveduto ad inoltrare la richiesta di consegna ai fattorini disponibili, per ulteriori informazioni sull'ordine, clicca il link sottostante.
 
	------------------------
	http://localhost/html_project/PROGETTO/Fornitori-Home.php

	";

	$headers = 'From:SnackyService@gmail.com' . "\r\n"; 
	mail($destinatario, $subject, $message, $headers);
}

function send_order_to_client($destinatario){
	$to      = $destinatario; 
	$subject = 'Ordine evaso! ';
	$message = "
 
	Il tuo ordine e' stato accettato!
	I nostri fattorini provvederanno a consegnarti il tuo ordine il prima possibile!
 
	------------------------
	http://localhost/html_project/PROGETTO/Snacky-Home.php

	";

	$headers = 'From:SnackyService@gmail.com' . "\r\n"; 
	mail($destinatario, $subject, $message, $headers);
}

//Restituisce il numero di ordini di un negozio inoltrati ma non ancora affidati ad un fattorino (Usata dal negozio per vedere lo stato dei suoi ordini)
function shop_check_ordini_non_spediti($idAccount){
	if ($stmt = $mysqli->prepare("SELECT idOrdine FROM ordine WHERE statoOrdine = ?  AND idNegozio = ?")) { 
		$stmt->bind_param('si', "Inoltrato",$idAccount); 
		$stmt->execute(); 
		$stmt->store_result();
		
		return $stmt->num_rows;
	}
}

//Restituisce il numero di ordini inoltrati ma non ancora affidati ad un fattorino (Usata dai fattorini per vedere consegne da fare)
function delivery_check_ordini_non_spediti(){
	if ($stmt = $mysqli->prepare("SELECT idOrdine FROM ordine WHERE statoOrdine = ?")) { 
		$stmt->bind_param('s', "Inoltrato"); 
		$stmt->execute(); 
		$stmt->store_result();
		
		return $stmt->num_rows;
	}
}

?>