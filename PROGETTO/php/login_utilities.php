<?php

function sec_session_start() {
        $session_name = 'sec_session_id'; 
        $secure = false; 
        $httponly = true; 
        ini_set('session.use_only_cookies', 1); 
        $cookieParams = session_get_cookie_params(); 
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
        session_name($session_name); 
        session_start(); 
        session_regenerate_id(); 
}

function login($username, $password, $mysqli) {
   global $errors ;
   if ($stmt = $mysqli->prepare("SELECT idAccount, username, password, salt, Abilitato, tipologia FROM accounts WHERE username = ? LIMIT 1")) { 
      $stmt->bind_param('s', $username); 
      $stmt->execute(); 
      $stmt->store_result();
      $stmt->bind_result($user_id, $username, $db_password, $salt, $abilitato, $tipologia); 
      $stmt->fetch();
      $password = hash('sha512', $password.$salt); 
      if($stmt->num_rows == 1) { 
         if(checkbrute($user_id, $mysqli) == true) { 
            $errors = "L'account è stato disabilitato a causa dei troppi tentativi di accesso.";
            return false;
         } else {
         if($db_password == $password) { 
		 
			if($abilitato){
            // Password corretta!            
               $user_browser = $_SERVER['HTTP_USER_AGENT']; 
               $user_id = preg_replace("/[^0-9]+/", "", $user_id); 
               $_SESSION['user_id'] = $user_id; 
               $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); 
               $_SESSION['username'] = $username;
			   $_SESSION['tipologia'] = $tipologia;
               $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
               // Login eseguito con successo.
			return true; 
			} else{
				$errors = "L'account non è ancora stato attivato, segui le istruzioni all'interno della email che hai ricevuto.";
			}
			
         } else {
            $errors = "La password non è corretta";
            $now = time();
            $mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_id', '$now')");
            return false;
         }
      }
      } else {
		  $errors = "L'utente selezionato non esiste";
         // L'utente inserito non esiste.
         return false;
      }
   }
}


function checkbrute($user_id, $mysqli) {
   // Recupero il timestamp
   $now = time();
   // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
   $valid_attempts = $now - (2 * 60 * 60); 
   if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) { 
      $stmt->bind_param('i', $user_id); 
      // Eseguo la query creata.
      $stmt->execute();
      $stmt->store_result();
      // Verifico l'esistenza di più di 5 tentativi di login falliti.
      if($stmt->num_rows > 5) {
         return true;
      } else {
         return false;
      }
   }
}


function login_check($mysqli) {
   // Verifica che tutte le variabili di sessione siano impostate correttamente
   if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'])) {
     $user_id = $_SESSION['user_id'];
     $login_string = $_SESSION['login_string'];
     $username = $_SESSION['username'];     
     $user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente.
     if ($stmt = $mysqli->prepare("SELECT password FROM accounts WHERE idAccount = ? LIMIT 1")) { 
        $stmt->bind_param('i', $user_id); // esegue il bind del parametro '$user_id'.
        $stmt->execute(); // Esegue la query creata.
        $stmt->store_result();
 
        if($stmt->num_rows == 1) { // se l'utente esiste
           $stmt->bind_result($password); // recupera le variabili dal risultato ottenuto.
           $stmt->fetch();
           $login_check = hash('sha512', $password.$user_browser);
           if($login_check == $login_string) {
              // Login eseguito!!!!
              return true;
           } else {
              //  Login non eseguito
              return false;
           }
        } else {
            // Login non eseguito
            return false;
        }
     } else {
        // Login non eseguito
        return false;
     }
   } else {
     // Login non eseguito
     return false;
   }
}

function logout(){
	sec_session_start();
	// Elimina tutti i valori della sessione.
	$_SESSION = array();
	// Recupera i parametri di sessione.
	$params = session_get_cookie_params();
	// Cancella i cookie attuali.
	setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
	// Cancella la sessione.
	session_destroy();
	unset($_COOKIE['username']);
    unset($_COOKIE['password']);
    setcookie('username', null, -1);
    setcookie('password', null, -1);
	header('Location: ../PROGETTO/Snacky-Login.php');
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

 ?>