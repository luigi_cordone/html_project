<?php
include 'php/login_utilities.php';
$servername="localhost";
$username ="root";
$password ="";
$database = "snackydb";
sec_session_start();	

if(isset($_POST['success'])){

				header('Location: Snacky-Payment.php');
				exit();
		
			
}
		

if (isset($_POST['delete'])) {
	$conn = new mysqli($servername, $username, $password, $database);
	if ($stmt = $conn->prepare("DELETE FROM carrello_prodotti WHERE idProdotto=". $_POST['delete'] ." AND idAccount=". $_SESSION['user_id'] .""));
	$stmt->execute(); 

}

		
?>

<!DOCTYPE html>
<html lang="it">

<head>
	<title>Snacky-Cart</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script src="scripts/notify.min.js"></script>
	<script src="scripts/scriptNotifyClienti.js"></script>
	<script src="scripts/scriptCart.js"></script>
	<link rel="stylesheet" type="text/css" href="css/cart.css">	
	<!-- Google Icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
</head>

<body>


<section class="login-block">
    <div class="container">
	<div class="row">
	
	
	
	<div class="container-fluid breadcrumbBox text-center">
			<ol class="breadcrumb">
				<li><a href="Snacky-Home.php">Continua ad acquistare</a></li>
				<li class="active"><a href="#">Ordine</a></li>
			</ol>
		</div>
		
	
		
		<div class="container text-center">

			<div style="display:block; margin-left:auto; margin-right:auto;">
				<img src="img/sprite.png" alt="Cart image">
			</div>
			
			<form action="Snacky-Payment.php" name='add' method="POST">

			<input type="submit" name="submit" id="subOrdina"  class="btn btn-success"  value="ORDINA!" style="width:70%; display:block; margin-left:auto; margin-right:auto;"/><br/><br/><br/>

				<label for="dropDown" class="text-uppercase">LUOGO E ORA DI CONSEGNA:</label>

			
			
			<div class="form-group">
			
			
			
			
			
				<select name='luogo' class="form-control" target=”Snacky-Payment.php” id="dropDown" required>
				<!--	<option disabled selected value> Luogo di consegna </option> -->
					<option value="Ingresso piano terra">Ingresso piano terra</option>
					<option value="Ingresso primo piano"> Ingresso primo piano</option>
					<option value="Punto Ristoro"> Punto Ristoro</option>
				</select>
			</div>
			
			

				<div>					
					<select name = 'ora' id="oraConsegna" required>
				<!--	<option disabled selected value> Orario di consegna </option>-->
					  <option value="09:00">09:00</option>
					  <option value="09:30">09:30</option>
					  <option value="10:00">10:00</option>
					  <option value="10:30">10:30</option>
					  <option value="11:00">11:00</option>
					  <option value="11:30">11:30</option>
					  <option value="12:00">12:00</option>
					  <option value="12:30">12:30</option>
					  <option value="13:00">13:00</option>
					  <option value="13:30">13:30</option>
					  <option value="14:00">14:00</option>
					  <option value="14:30">14:30</option>
					  <option value="15:00">15:00</option>
					  <option value="15:30">15:30</option>
					  <option value="16:00">16:00</option>
					  <option value="16:30">16:30</option> 
					  <option value="17:00">17:00</option>
					  <option value="17:30">17:30</option>
					  <option value="18:00">18:00</option>
					  <option value="18:30">18:30</option>
					
					</select>
				
				</form>
				
			</div><br/>
				
			<div class="container" style='width:100%;margin-left:auto;margin-right:auto;display:block;'>
					
				<?php
					
					$servername="localhost";
					$username ="root";
					$password ="";
					$database = "snackydb";

					$sommaTot=0;

					echo "<table  class= 'table table-responsive table-striped table-bordered' style='width:42%;margin-left:auto;margin-right:auto;display:block;' id='tbl'>";
					echo "<tr><th>Quantità</th><th>Descrizione</th><th>Prezzo</th><th></th></th>";

					$conn = new mysqli($servername, $username, $password, $database);
					
					$result = $conn->query("SELECT idProdotto, nomeProdotto,prezzoUnitario, SUM(quantita) AS qta
											FROM carrello_prodotti
											WHERE idAccount=" . $_SESSION['user_id'] . "
											GROUP BY idProdotto");
					
					if($result->num_rows > 0){
						while($row = $result->fetch_assoc() ) {
							
							$totale = $row["qta"] * $row["prezzoUnitario"];
							$sommaTot= $sommaTot + $totale; 
							echo "<tr><td>" . $row["qta"] . "</td><td>" . $row["nomeProdotto"] . "</td><td>" . $row["prezzoUnitario"] . ",00€</td><td><form action=# method=POST><button type=submit name=delete class= 'btn btn-danger' value=". $row["idProdotto"] ." style='display:block;margin-left:auto;margin-right:auto;'>Rimuovi</button></form></td></tr>";
							
						}
					}else{
						
						echo '<script>document.getElementById("subOrdina").disabled=true</script>';
  
						
					}
					
					echo "</table>";
					
					
					echo "<span style='float:left;'> TOTALE " . $sommaTot . ",00€ </span>";
					$conn->close();
				?>
			</div>
					
				
			
			
			

		</div>
		
		
		
			
		
	

    </div>
  </div>
          
</section>

	<nav class="navbar navbar-expand-lg navbar-dark fixed-top text-white" style="opacity:0.9;">	
	
		<!-- E'IL NOME DEL NOSTRO SITO CLICCABILE CHE FA TORNARE ALLA HOME  -->
		<a class="navbar-brand text-white" href="Snacky-Home.php"><img alt="Snacky logo" src="./img/NH4apoE.png" width="180" height="65"></a>
		
		<!-- SERVE PER FAR APPARIRE IL BOTTONE IN CASO DI RIDIMENSIONAMENTO -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		 <!-- NAVBAR  -->
		 <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
		<li class="nav-item">
				<div class="dropdown show">
				<a class="nav-link text-white decorated-linkText-nav" href="Cart.php" title="Prodotti nel carrello">
				<button  type="button" class="btn btn-light cart btn btn-danger" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <!-- In formaction devo specificare URL dove andare. Ho usato button per metter l'Img-->
					<i id ="icon" class="material-icons Responsive">announcement</i>
					<div id = "space" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
			

  </div>
				</div>
		
				</button> 
				
				
				</a>
				
				
				</li>
			 
			  <li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Cart.php" title="Prodotti nel carrello">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">local_grocery_store</i>
					</button>
					
				</a>
				</li>
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Home.php" title="Vai alla Home">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">account_box</i>
					</button>
					
				</a>
				</li>
		
				<li class="nav-item">
				<a class="nav-link text-white decorated-linkText-nav" href="Snacky-Logout.php" title="Effettua il Logout">
					<button type="submit" class="btn btn-light cart" >
						<i class="material-icons Responsive">exit_to_app</i>
					</button>
					
				</a>
				</li>
		
				
			</ul>
		 </div>
	</nav>


	
	

	
	
	<!-- Footer -->
    <footer class="py-3 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white medium">Copyright Snacky</p>
      </div>
    </footer>



</body>
</html>
